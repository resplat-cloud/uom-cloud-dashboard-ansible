# uom-cloud-dashboard-ansible

This repository was forked from [NeCTAR-RC/nectar-dashboard-ansible](https://github.com/NeCTAR-RC/nectar-dashboard-ansible/).

Is intended to automatically install the UoM Dashboard located here:
[resplat-cloud/uom-cloud-dashboard](https://gitlab.unimelb.edu.au/resplat-cloud/uom-cloud-dashboard/-/tree/uom/2023.2?ref_type=heads)

This playbook has been tested to be stable with the following configuration:
```
Flavor : uom.general.2c8g (2 VCPU, 8GB RAM, 30GB disk)
Image  : NeCTAR Ubuntu 22.04 LTS (Focal) amd64
Ports  : ssh, http (limit IP ranges to prevent public access)
```

For contributing to the
[resplat-cloud/uom-cloud-dashboard](https://gitlab.unimelb.edu.au/resplat-cloud/uom-cloud-dashboard)
repository please see the
[Contribution Guide](https://gitlab.unimelb.edu.au/resplat-cloud/uom-cloud-dashboard/wikis/Contribution-guide).

----
### Install dependencies

you need some dependencies on the host system

```
sudo apt update
sudo apt install -y ansible git vim python3-pip gettext python3-pymysql python-is-python3

sudo bash
  pip install python-memcached
exit

git clone https://gitlab.unimelb.edu.au/resplat-cloud/uom-cloud-dashboard-ansible.git
```

on the new directory `uom-cloud-dashboard-ansible/`

```
cd uom-cloud-dashboard-ansible
```

Check that you checkout the latest version `uom/2023.2`

```
git checkout uom/2023.2
```

### Configure the playbook

on the new directory `uom-cloud-dashboard-ansible/`

Copy **roles/horizon/defaults/main.yml.example**.

```
cp roles/horizon/defaults/main.yml.example roles/horizon/defaults/main.yml
```

Edit, add settings if necessary. 

```
vi roles/horizon/defaults/main.yml
```

All configuration should go here **main.yml** before running the playbook, otherwise you would need to update `/etc/horizon/local_settings.py` manually.

Ansible will fill in `horizon_server_name` with the server ip, add any other configuration if necessary

See the README.md in the
[resplat-cloud/uom-cloud-dashboard](https://gitlab.unimelb.edu.au/resplat-cloud/uom-cloud-dashboard)
repository for additional guidance on configuring the settings.

### Enabling SSL

SSL certificated is required for SSO authentication, refer to MRC documentation on how to obtain your SSL certificate from DigiCert.

The cetificate doesn't go on this server, the UoM dashboard is deployed as high availability application, on production there is a Proxy/loadbalancer as front-end and two hosts with the dashboard as back-end.

So the certificate goes on the Proxy/loadbalancer not on the host deployed here, details on the Proxy configuration are on a section furhter below.


### Run playbook

```
sudo ansible-playbook -v -i hosts -c local site.yml
```

### Test

- Go to `http://<hostname>`.
- Enter your keystone username & password to log in.

### Default locations for development

| Location | Description |
| --- | --- |
| /opt/dashboard_code/ | Location where the dashboard is cloned to, to later install from there |
| /opt/horizon/lib/python3.10/site-packages/openstack_dashboard/ | Installed Horizon location |
| /opt/horizon/lib/python3.10/site-packages/nectar_dashboard/ | Installed Nectar dashboard location |
| /etc/horizon/local_settings.py | Installed local_settings.py file |
| /opt/horizon/bin/horizon-manage.py | For running Django commands. |
| /var/log/horizon/ | Location of logs. |

### roles/horizon/defaults/main.yml variables target files

| Variable | Location |
| --- | --- |
| horizon_server_name | roles/horizon/templates/openstack_dashboard.conf.j2 <br> roles/horizon/templates/uom_local_settings.py.j2 |
| horizon_secret_key | roles/horizon/templates/uom_local_settings.py.j2 |
| allocations_keystone_u | roles/horizon/templates/uom_local_settings.py.j2 |
| horizon_keystone_host | roles/horizon/templates/uom_local_settings.py.j2 |
| horizon_rcshibboleth_url | roles/horizon/templates/uom_local_settings.py.j2 |
| uom_access_role_id | roles/horizon/templates/uom_local_settings.py.j2 |
| nova_az_filter | roles/horizon/templates/uom_local_settings.py.j2 |
| horizon_database_engine | roles/horizon/templates/uom_local_settings.py.j2 <br> roles/horizon/tasks/horizon_apache.yml <br> roles/horizon/tasks/horizon_db_setup.yml |
| horizon_mysql_user | roles/horizon/templates/uom_local_settings.py.j2 <br> roles/horizon/tasks/horizon_db_setup.yml |
| horizon_site_name | (not found?) |
| UoM dashboard - dashboard customization |
| uom_cloud_dashboard_path | roles/horizon/templates/uom_local_settings.py.j2 <br> roles/horizon/tasks/horizon_post_install.yml <br> roles/horizon/tasks/horizon_install.yml |
| uom_cloud_dashboard_version | roles/horizon/tasks/horizon_install.yml |
|  |  |

---

# Notes


## Official doc

https://docs.openstack.org/horizon/latest/configuration/themes.html

Changing the Site Title

Att: SITE_BRANDING <br>
File: openstack_dashboard/local/local_settings.py.

Changing the theme

Att: AVAILABLE_THEMES <br>
File: openstack_dashboard/local/local_settings.py.<br>
details:<br>
list of tuples, such that ('name', 'label', 'path') <br>
path :The directory location for the theme. The path must be relative to the openstack_dashboard directory or an absolute path to an accessible location on the file system

## Where the configuration variables come from

file uom-cloud-dashboard-ansible/roles/horizon/templates/uom_local_settings.py.j2 calls
```
from openstack_dashboard.settings import HORIZON_CONFIG
POLICY_FILES_PATH = "{{ dashboard_code_path }}/policy"
```

seems to come from 
```
/opt/horizon/lib/python3.10/site-packages/openstack_dashboard/settings.py:60:HORIZON_CONFIG = {
```

## Where are the Local settings set

The playbook creates a simlink

```
/opt/horizon/lib/python3.10/site-packages/openstack_dashboard/local/local_settings.py -> /etc/horizon/local_settings.py
```

## Where is the theme set

The theme is here
```
/opt/dashboard_code/theme-uom
```

The theme is set on local_settings /etc/horizon/local_settings.py

```
AVAILABLE_THEMES = [
    ('default', 'Default', 'themes/default'),
    ('nectar', 'Nectar', '/opt/dashboard_code/theme'),
    ('custom', 'Custom', '/opt/dashboard_code/theme-uom'),
]

SELECTABLE_THEMES = [
    ('custom', 'Custom', '/opt/dashboard_code/theme-uom'),
]

DEFAULT_THEME = 'custom'

```

## Static files

on apache configuration "Alias" '/static' should point to the full path of 'dashboard/theme/static/'

```
/etc/apache2/sites-available/openstack-dashboard.conf:11:    WSGIScriptAlias / /opt/horizon/lib/python3.10/site-packages/openstack_dashboard/wsgi/django.wsgi
/etc/apache2/sites-available/openstack-dashboard.conf:24:    Alias /static /opt/horizon/lib/python3.10/site-packages/static/
```


# Where/how the authentication is set

The configuration is created from `roles/horizon/templates/horizon_local_settings.py.j2` , looking for the variables
- horizon_rcshibboleth_url, if is defined
- disable_aaf, is is not disabled

On local_settings.py /etc/horizon/local_settings.py
```
# Enables keystone web single-sign-on if set to True.
WEBSSO_ENABLED = True
#OPENSTACK_SSL_NO_VERIFY = True

# Shibboleth URL
HORIZON_CONFIG['rcshibboleth_url'] = "https://accounts.test.rc.nectar.org.au/Shibboleth.sso/UniMelb?target=%s" % urllib.parse.quote("https://accounts.test.rc.nectar.org.au/login/?return-path=https://dashboard-next.test.cloud.unimelb.edu.au/auth/websso/")
```

Also note the host needs to be white listed on the DB Proxy

For test on:
puppet-site-melbournetest/data/softwaregroups/dbproxy.yaml

## Where the apache configuration is created from

Apache configuration is created from `roles/horizon/tasks/horizon_apache.yml` the variables are set on `roles/horizon/defaults/main.yml`.

## DB Configuration

The DB configuration is set on `roles/horizon/defaults/main.yml.example` on the DB info section, the installation puts it on `/etc/openstack-dashboard/local_settings.py`.

Set the DB details here `roles/horizon/defaults/main.yml.example`
```
## DB info
horizon_database_engine: django.db.backends.mysql
...
```

And verify the DB details are correct for your test environment on `local_settings.py`

```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'uom_dashboard_test_next',
        'HOST': 'dbproxyvip-test-qh2.os.cloud.unimelb.edu.au',
        'USER': 'dashboard_test',
        'PASSWORD': '7pEP4gSB2jY8',
        'PORT': '',
     },
}

```
# Dashboard Next Test configuration

## Test Next DNS IP record
https://dashboard-next.test.cloud.unimelb.edu.au/ <br>

```
Name:	dashboard-next.test.cloud.unimelb.edu.au
Address: 115.146.82.70

```

## Test Next HA Proxy/loadbalancer configuration

/review-nectar/puppet-site-melbournetest/data/softwaregroups/haproxy.yaml

```
    ipaddresses:
      - '172.26.133.149'
      - '172.26.129.14'
    ports: '80'
    server_names:
      - 'vm-172-26-133-149.rc.cloud.unimelb.edu.au'
      - 'vm-172-26-129-14.rc.cloud.unimelb.edu.au'
    sslfile: '/etc/ssl/private/dashboard-next_test_cloud_unimelb_edu_au.pem'
    track_script: 'check_haproxy'
    virtual_ip: '115.146.82.70'
    virtual_router_id: 12
```

## Test Next DB Proxy configuration

review-nectar/puppet-site-melbournetest/data/softwaregroups/dbproxy.yaml

```
nectar::role::db::proxy::external_hosts:
  - '115.146.83.0/27' # Core Services test
  - '115.146.83.128/26' # Core Services
  - '172.26.22.0/24'  # Core Services test
  - '192.43.209.8/30' # Core Services
  - '172.26.133.149/32' # Sergio test VM for dashboard
  - '172.26.129.14/32'  # Khalid test VM for dashboard

```

# local_settings.py configuration

Refer to https://gitlab.unimelb.edu.au/resplat-public/ops-doc/-/wikis/Cloud/Tasks/Openstack-Dashboard-Configuration 

