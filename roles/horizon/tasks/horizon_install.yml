---
# Copyright 2014, Rackspace US, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

- name: horizon install - Ensure the system group
  group:
    name: "{{ horizon_system_group_name }}"
    state: "present"
    system: "yes"
  tags:
    - horizon-group

- name: horizon install - Ensure the horizon system user
  user:
    name: "{{ horizon_system_user_name }}"
    group: "{{ horizon_system_group_name }}"
    state: "present"
    system: "yes"
  tags:
    - horizon-user

- name: horizon install - Find home directory for system user
  shell: 'grep {{ horizon_system_user_name }} /etc/passwd | cut -d ":" -f6'
  register: find_horizon_system_user_home
  changed_when: false
  tags:
    - horizon-db-setup
    - horizon-user

- name: horizon install - Fail if no home directory found for system user
  when: 'find_horizon_system_user_home.stdout == ""'
  fail:
    msg: "User '{{ horizon_system_user_name }}' has no home directory."
  tags:
    - horizon-db-setup
    - horizon-user

- name: horizon install - Set fact for system user home directory
  set_fact:
    horizon_system_user_home: "{{ find_horizon_system_user_home.stdout }}"
  tags:
    - horizon-db-setup
    - horizon-user

- name: horizon install - Create horizon dir
  file:
    path: "{{ item.path }}"
    state: "directory"
    owner: "{{ item.owner|default(horizon_system_user_name) }}"
    group: "{{ item.group|default(horizon_system_group_name) }}"
  with_items:
    - { path: "{{ horizon_venv_path }}", mode: "0775", owner: "{{ horizon_system_user_name }}", group: "{{ horizon_system_group_name }}" }
    - { path: "/etc/horizon", mode: "2775" }
    - { path: "{{ horizon_system_user_home }}", mode: "2775" }
  tags:
    - horizon-dirs

- name: horizon install - Create horizon venv dir
  file:
    path: "{{ item.path }}"
    state: directory
  with_items:
    - { path: "{{ horizon_venv_path }}" }
#    - { path: "{{ horizon_venv_path }}/bin" }
  when: horizon_venv_enabled | bool
  tags:
    - horizon-dirs

- name: horizon install - Update apt sources
  apt:
    update_cache: yes
    cache_valid_time: 600
  register: apt_update
  until: apt_update is success
  retries: 5
  delay: 2
  tags:
    - horizon-apt-packages

- name: horizon install - Install apt packages
  apt:
    pkg: "{{ horizon_apt_packages }}"
    state: latest
  register: install_packages
  until: install_packages is success
  retries: 3
  delay: 2
  tags:
    - horizon-install
    - horizon-apt-packages

- name: horizon install - Install argparse from pip
  pip:
    name: argparse==1.1
    executable: pip3
  tags:
    - horizon-install

# git-review must be version 1.28 or higher to be compatible with Gerrit,
# and installation from apt gives version 1.26 as of 16 Apr 2020
- name: horizon install - Install git-review from pip
  pip:
    name: git-review>=1.28
    executable: pip3
  tags:
    - horizon-install

# Address AttributeError: module 'dns.rdtypes' has no attribute 'ANY',
# at a time of running ./manage.py compilemessages
- name: horizon install - Install eventlet==0.33.3 from pip
  pip:
    name: eventlet==0.33.3 
    executable: pip3
  tags:
    - horizon-install

- name: horizon install - Git clone dashboard repo and checkout version
  git:
    repo: "{{ dashboard_repo }}"
    dest: "{{ dashboard_code_path }}"
    version: "{{ dashboard_version }}"
  register: git_pull_result
  notify: Restart apache2
  tags:
    - horizon-dirs
    - horizon-git-pull

- name: horizon install - Ensure ownership (recurse) for installation directory before install
  file:
    path: "{{ horizon_venv_path }}"
    state: directory
    owner: "{{ horizon_system_user_name }}"
    group: "{{ horizon_system_group_name }}"
    recurse: yes
  tags:
    - horizon-git-pull

- name: horizon install - Create horizon venv and install pip packages (venv)
  pip:
    requirements: "{{ dashboard_code_path }}/requirements.txt"
    state: present
    virtualenv: "{{ horizon_venv_path }}"
    virtualenv_command: /usr/bin/python3 -m venv
    virtualenv_site_packages: True
    extra_args: "-c {{ dashboard_code_path }}/upper-constraints.txt -r {{ dashboard_code_path }}/test-requirements.txt" 
  register: install_packages
  until: install_packages is success
  retries: 5
  delay: 2
  when:
    - horizon_venv_enabled | bool
  notify: Restart apache2
  tags:
    - horizon-pip-packages

- name: horizon install - Install pip packages (no venv)
  pip:
    requirements: "{{ horizon_venv_path }}/requirements.txt"
    state: present
    extra_args: "-c {{ horizon_venv_path }}/developer-constraints.txt"
  register: install_packages
  until: install_packages is success
  retries: 5
  delay: 2
  when: not horizon_venv_enabled | bool
  notify: Restart apache2
  tags:
    - horizon-pip-packages

- name: horizon install - Install dashboard as a pip package
  pip:
    name: "{{ dashboard_code_path }}"
    state: present
    virtualenv: "{{ horizon_venv_path }}"
    virtualenv_command: /usr/bin/python3 -m venv
    virtualenv_site_packages: True
    editable: False
  until: install_packages is success
  retries: 5
  delay: 2
  when:
    - horizon_venv_enabled | bool
  notify: Restart apache2
  tags:
    - horizon-pip-packages

# No Needed for Horizon default installation
#- name: Symlink for uom-cloud-dashboard custom theme
#  file:
#    src: "{{ uom_cloud_dashboard_path }}/theme"
#    dest: "{{ horizon_lib_dir }}/openstack_dashboard/themes/custom"
#    owner: "{{ horizon_system_user_name }}"
#    group: "{{ horizon_system_group_name }}"
#    state: "link"
#  notify: Restart apache2

- name: horizon install - Create static horizon dir
  file:
    path: "{{ item.path }}"
    state: "directory"
    owner: "{{ item.owner|default(horizon_system_user_name) }}"
    group: "{{ item.group|default(horizon_system_group_name) }}"
  with_items:
    - { path: "/etc/horizon", mode: "2775" }
    - { path: "{{ horizon_lib_dir }}/static", mode: "2775" }
    - { path: "{{ horizon_lib_dir }}/openstack_dashboard", mode: "2775" }
    - { path: "{{ horizon_lib_dir }}/openstack_dashboard/local", mode: "2775" }
  tags:
    - horizon-dirs

- name: horizon install - Test for log directory or link
  shell: |
    if [ -h "/var/log/horizon"  ]; then
      chown -h {{ horizon_system_user_name }}:{{ horizon_system_group_name }} "/var/log/horizon"
      chown -R {{ horizon_system_user_name }}:{{ horizon_system_group_name }} "$(readlink /var/log/horizon)"
      chown -h {{ horizon_system_user_name }}:{{ horizon_system_group_name }} "/var/log/uom-cloud-dashboard"
      chown -R {{ horizon_system_user_name }}:{{ horizon_system_group_name }} "$(readlink /var/log/uom-cloud-dashboard)"
    else
      exit 1
    fi
  register: log_dir
  failed_when: false
  changed_when: log_dir.rc != 0
  tags:
    - horizon-dirs
    - horizon-logs

- name: horizon install - Create horizon log dir
  file:
    path: "{{ item }}"
    state: directory
    owner: "{{ horizon_system_user_name }}"
    group: "{{ horizon_system_group_name }}"
    mode: "0775"
  with_items:
    - /var/log/horizon
#    - /var/log/uom-cloud-dashboard
  when: log_dir.rc != 0
  tags:
    - horizon-dirs
    - horizon-logs

#- name: horizon install - Create uom-cloud-dashboard logs
#  file:
#    path: "/var/log/uom-cloud-dashboard/{{ item }}"
#    state: touch
#    owner: "{{ horizon_system_user_name }}"
#    group: "{{ horizon_system_group_name }}"
#    mode: "0644"
#  with_items:
#    - debug
#    - provisioning.log
